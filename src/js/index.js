import header from '../components/header/header';
import footer from '../components/footer/footer';
import poster from '../components/poster/poster';

document.addEventListener('DOMContentLoaded', () => {
  header();
  footer();
  poster();
});
