const buildPath = './build';
const srcPath = './src';

module.exports = {
  buildPath,
  srcPath,
  build: {
    images: `${buildPath}/assets/images/`,
    fonts: `${buildPath}/assets/fonts/`,
    fonticon: `${buildPath}/assets/fonticon/`,
    sprite: `${buildPath}/assets/sprite/`,
    styles: `${buildPath}/css/`,
    scripts: `${buildPath}/js/`,
    html: `${buildPath}/`,
  },
  src: {
    twig: `${srcPath}/**/*.twig`,
    styles: `${srcPath}/**/*.scss`,
    scripts: `${srcPath}/**/*.js`,
    images: `${srcPath}/assets/images/**/*.{jpg,png,svg,ico,gif,webp}*`,
    fonts: `${srcPath}/assets/fonts/`,
    sprite: `${srcPath}/assets/sprite/*.svg`,
    fonticon: `${srcPath}/assets/fonticon/*.svg`,
    entryJS: `${srcPath}/js/index.js`,
  },
};
