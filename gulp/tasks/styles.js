const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');

const mode = require('../mode');
const paths = require('../paths');
const logger = require('../logger');

const styles = () => {
  return gulp
    .src(`${paths.srcPath}/scss/index.scss`)
    .pipe(mode.development(sourcemaps.init()))
    .pipe(sass({ outputStyle: 'compressed' }))
    .pipe(autoprefixer('last 2 versions', '> 1%', 'ie 10'))
    .pipe(mode.production(rename({ suffix: '.min' })))
    .pipe(mode.development(sourcemaps.write()))
    .pipe(logger('stylesheets'))
    .pipe(gulp.dest(paths.build.styles));
};

module.exports = styles;
