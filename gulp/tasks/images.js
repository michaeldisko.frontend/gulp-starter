const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const newer = require('gulp-newer');
const size = require('gulp-size');
const cached = require('gulp-cached');

const paths = require('../paths');
const mode = require('../mode');
const logger = require('../logger');

const imageMinConfig = {
  jpegtran: { progressive: true },
  optipng: { optimizationLevel: 5 },
  svgo: {
    plugins: [{ removeViewBox: false }, { cleanupIDs: false }],
  },
};

const images = () => {
  return gulp
    .src(paths.src.images)
    .pipe(cached('images'))
    .pipe(newer(paths.build.images))
    .pipe(
      mode.production(
        imagemin([
          imagemin.jpegtran(imageMinConfig.jpegtran),
          imagemin.optipng(imageMinConfig.optipng),
          imagemin.svgo(imageMinConfig.svgo),
        ])
      )
    )
    .pipe(size({ showFiles: true }))
    .pipe(logger('images'))
    .pipe(gulp.dest(paths.build.images));
};

module.exports = images;
