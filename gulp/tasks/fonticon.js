const gulp = require('gulp');
const iconfont = require('gulp-iconfont');
const iconfontCss = require('gulp-iconfont-css');

const paths = require('../paths');
const logger = require('../logger');

const fonticon = () => {
  const runTimestamp = Math.round(Date.now() / 1000);

  return gulp
    .src([paths.src.fonticon], { base: 'src' })
    .pipe(
      iconfontCss({
        path: 'src/scss/fonticon/_template.scss',
        targetPath: '../../../src/scss/fonticon/_fonticon.scss',
        fontPath: '../assets/fonticon/',
        fontName: 'icons',
        cacheBuster: runTimestamp,
      })
    )
    .pipe(
      iconfont({
        fontName: 'icons',
        timestamp: runTimestamp,
        formats: ['svg', 'ttf', 'eot', 'woff', 'woff2'],
        normalize: true,
        fontHeight: 1000,
      })
    )
    .pipe(logger('fonticon'))
    .pipe(gulp.dest(paths.build.fonticon));
};

module.exports = fonticon;
