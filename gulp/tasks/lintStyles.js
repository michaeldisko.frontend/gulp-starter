const gulp = require('gulp');
const gulpStylelint = require('gulp-stylelint');

const paths = require('../paths');
const logger = require('../logger');
const mode = require('../mode');

const lintStyles = () => {
  return gulp
    .src(paths.src.styles)
    .pipe(
      gulpStylelint({
        failAfterError: mode.production(),
        reporters: [{ formatter: 'string', console: true }],
      })
    )
    .pipe(logger('scss-linter'));
};

module.exports = lintStyles;
