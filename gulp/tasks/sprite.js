const gulp = require('gulp');
const svgSprite = require('gulp-svg-sprite');
const svgmin = require('gulp-svgmin');
const cheerio = require('gulp-cheerio');
const replace = require('gulp-replace');

const paths = require('../paths');
const logger = require('../logger');

const sprite = () => {
  return gulp
    .src([paths.src.sprite])
    .pipe(svgmin())
    .pipe(
      cheerio({
        run: $ => {
          $('[fill]').removeAttr('fill');
          $('[stroke]').removeAttr('stroke');
          $('[style]').removeAttr('style');
        },
        parserOptions: { xmlMode: true },
      })
    )
    .pipe(replace('&gt;', '>'))
    .pipe(
      svgSprite({
        shape: {
          spacing: {
            padding: 0,
            box: 'border',
          },
        },
        mode: {
          symbol: {
            sprite: '../sprite.svg',
          },
        },
      })
    )
    .pipe(logger('svg-sprite'))
    .pipe(gulp.dest(paths.build.sprite));
};

module.exports = sprite;
