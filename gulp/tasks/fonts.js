const gulp = require('gulp');
const cached = require('gulp-cached');

const paths = require('../paths');
const logger = require('../logger');

const fonts = () => {
  return gulp
    .src(paths.src.fonts)
    .pipe(cached('fonts-cache'))
    .pipe(logger('fonts'))
    .pipe(gulp.dest(paths.build.fonts));
};

module.exports = fonts;
