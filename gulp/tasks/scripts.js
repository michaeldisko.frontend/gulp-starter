const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const babelify = require('babelify');
const uglify = require('gulp-uglify');

const paths = require('../paths');
const mode = require('../mode');

const scripts = () => {
  return browserify({ entries: ['./src/js/index.js'] })
    .transform(babelify, { presets: ['@babel/preset-env'] })
    .bundle()
    .pipe(source('index.js'))
    .pipe(
      mode.production(
        rename({
          suffix: '.min',
        })
      )
    )
    .pipe(buffer())
    .pipe(mode.development(sourcemaps.init()))
    .pipe(mode.production(uglify()))
    .pipe(mode.development(sourcemaps.write()))
    .pipe(gulp.dest(paths.build.scripts));
};

module.exports = scripts;
