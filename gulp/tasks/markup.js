const gulp = require('gulp');
const twig = require('gulp-twig');

const paths = require('../paths');
const logger = require('../logger');

const markup = () => {
  return gulp
    .src('./src/pages/*.twig')
    .pipe(
      twig({
        data: {
          title: 'Starter Kit',
        },
      })
    )
    .pipe(logger('markup'))
    .pipe(gulp.dest(paths.build.html));
};

module.exports = markup;
