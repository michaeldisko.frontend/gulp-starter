const gulp = require('gulp');
const eslint = require('gulp-eslint');

const paths = require('../paths');
const logger = require('../logger');
const mode = require('../mode');

const lintScipts = () => {
  return gulp
    .src(paths.src.scripts)
    .pipe(eslint())
    .pipe(logger('js-linter'))
    .pipe(eslint.format())
    .pipe(mode.production(eslint.failAfterError()));
};

module.exports = lintScipts;
