const print = require('gulp-print').default;

module.exports = prefix => print(filepath => `${prefix}: ${filepath}`);
