const { series, parallel, watch } = require('gulp');
const requireDir = require('require-dir');
const browserSync = require('browser-sync').create();

const tasks = requireDir('./gulp/tasks', { recurse: true });
const paths = require('./gulp/paths');

const serve = () => {
  return browserSync.init({
    index: 'index.html',
    server: 'build',
    notify: false,
    open: true,
    cors: true,
    ui: false,
    host: 'localhost',
    port: process.env.PORT || 8080,
  });
};

const watcher = done => {
  watch(paths.src.twig).on('change', series(tasks.markup, browserSync.reload));
  watch(paths.src.styles).on(
    'change',
    series(
      tasks.lintStyles,
      tasks.styles,
      browserSync.reload.bind(null, `${paths.build.styles}/index.css`)
    )
  );
  watch(paths.src.scripts).on(
    'change',
    series(tasks.lintScripts, tasks.scripts, browserSync.reload)
  );
  watch(paths.src.images, tasks.images);
  watch(paths.src.fonts, tasks.fonts);
  watch(paths.src.fonticon, tasks.fonticon);
  watch(paths.src.sprite, tasks.sprite);

  done();
};

exports.lint = parallel(tasks.lintScripts, tasks.lintStyles);

exports.dev = series(
  tasks.clean,
  tasks.fonticon,
  tasks.sprite,
  tasks.images,
  tasks.fonts,
  parallel(tasks.markup, tasks.styles, tasks.scripts),
  watcher,
  serve
);

exports.build = series(
  tasks.clean,
  tasks.fonticon,
  tasks.sprite,
  tasks.images,
  tasks.fonts,
  parallel(
    tasks.lintStyles,
    tasks.lintScripts,
    tasks.markup,
    tasks.styles,
    tasks.scripts
  )
);
